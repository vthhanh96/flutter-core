import 'package:dio/dio.dart';
import 'package:flutter_core/http/response/raw_response.dart';
import 'package:res_string/res_string.dart';

class HttpError extends DioError {
  String? title;
  String? errorKey;
  String? errorMessage;

  HttpError(RequestOptions requestOptions, {this.title, this.errorMessage, this.errorKey}) : super(requestOptions: requestOptions);

  factory HttpError.parse(DioError error) {
    if (error.response?.data is Map<String, dynamic>) {
      return HttpError.fromDataResponse(error.requestOptions, error.response?.data);
    }

    return HttpError.fromDioErrorType(error.requestOptions, error.type, error.message);
  }

  factory HttpError.fromDataResponse(RequestOptions requestOptions, Map<String, dynamic> map) {
    final RawResponse rawResponse = RawResponse.fromJson(map);
    if (rawResponse.reply != null) {
      return HttpError.fromReplyData(requestOptions, rawResponse);
    }
    return HttpError.fromErrorKey(requestOptions, rawResponse.result);
  }

  factory HttpError.fromErrorKey(RequestOptions requestOptions, String? errorKey) {
    return HttpError(requestOptions, errorKey: errorKey, errorMessage: ResString.getStringFromServerLocalization(errorKey ?? ''));
  }

  factory HttpError.fromReplyData(RequestOptions requestOptions, RawResponse rawResponse) {
    final Map<String, dynamic> reply = rawResponse.reply ?? <String, dynamic>{};
    final String? title = ResString.getStringFromServerLocalization(reply['title'] ?? '');
    final String? errorMessage = ResString.getStringFromServerLocalization(reply['message'] ?? '');
    return HttpError(requestOptions, title: title, errorKey: rawResponse.result, errorMessage: errorMessage);
  }

  factory HttpError.fromDioErrorType(RequestOptions requestOptions, DioErrorType errorType, String errorMessage) {
    String errorKey = '';
    switch (errorType) {
      case DioErrorType.receiveTimeout:
        errorKey = 'error_receive_timeout';
        break;
      case DioErrorType.connectTimeout:
        errorKey = 'error_connect_timeout';
        break;
      case DioErrorType.sendTimeout:
        errorKey = 'error_send_timeout';
        break;
      case DioErrorType.response:
        errorKey = 'error_response';
        break;
      case DioErrorType.cancel:
      case DioErrorType.other:
        break;
    }
    return HttpError(requestOptions, errorKey: errorKey, errorMessage: errorMessage);
  }

  @override
  String toString() {
    return '''HTTP ERROR:
    title: $title,
    errorKey: $errorKey,
    message: $errorMessage''';
  }
}
