class RequestOptionExtraFields {
  static String retryCounter = 'retry_counter';
  static String requireToken = 'require_token';
  static String connectTimeout = 'connect_timeout';
  static String receiveTimeout = 'receive_timeout';
  static String sendTimeout = 'send_timeout';
  static String exchangeToken = 'exchangeToken';
}