import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_core/constant/server_const.dart';
import 'package:flutter_core/flutter_core.dart';
import 'package:flutter_core/http/adapter/http_client_adapter.dart';
import 'package:flutter_core/http/http_error.dart';
import 'package:flutter_core/http/interceptors/retry_interceptor.dart';
import 'package:flutter_core/http/interceptors/retry_when_error_access_token_interceptor.dart';
import 'package:flutter_core/http/request_option_extra_fields.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

typedef GetAccessToken = Future<String?> Function(bool shouldRefreshIfNeed);
typedef ExchangeToken = Future<void> Function();

dynamic _parseAndDecode(String response) {
  return jsonDecode(response);
}

dynamic parseJson(String text) {
  return compute(_parseAndDecode, text);
}

class HttpClientParams {
  int connectTimeout = 30000;
  int receiveTimeout = 30000;
  int sendTimeout = 30000;
  int retryCounter = 1;
  bool verbose = true;
  String apiVersion = '';
  int appType = 0;
  DeviceInfo? deviceInfo;
  AppInfo? appInfo;
}

class HttpClient {
  final Dio dio;
  final GetAccessToken getAccessToken;
  final HttpClientParams httpClientParams;

  HttpClient({
    required this.dio,
    required this.httpClientParams,
    required this.getAccessToken,
    required ExchangeToken exchangeToken,
    required CacheOptions cacheOptions,
    Interceptor? handleSpecificErrorInterceptor,
    Logger? logger,
  }) {
    dio.options = BaseOptions(
      connectTimeout: httpClientParams.connectTimeout,
      receiveTimeout: httpClientParams.receiveTimeout,
      sendTimeout: httpClientParams.sendTimeout,
    );
    dio.httpClientAdapter = DioHttpClientAdapter.platform.createHttpClientAdapter();
    dio.transformer = DefaultTransformer(jsonDecodeCallback: parseJson);

    dio.interceptors.add(DioCacheInterceptor(options: cacheOptions));
    dio.interceptors.add(RetryWhenErrorAccessTokenInterceptor(dio, exchangeToken));
    if (handleSpecificErrorInterceptor != null) {
      dio.interceptors.add(handleSpecificErrorInterceptor);
    }
    dio.interceptors.add(RetryInterceptor(dio, defaultRetryRequestCounter: httpClientParams.retryCounter));
    dio.interceptors.add(InterceptorsWrapper(onRequest: _onRequest, onResponse: _onResponse, onError: _onError));
    if (logger != null) {
      if (httpClientParams.verbose) {
        dio.interceptors.add(LogInterceptor(logPrint: logger.v));
      } else {
        dio.interceptors.add(LogInterceptor(logPrint: logger.d, responseBody: true, requestBody: true));
      }
    }
  }

  Future<dynamic> _onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    Map<String, dynamic> extra = options.extra;
    final bool requireToken = extra[RequestOptionExtraFields.requireToken] ?? true;
    if (requireToken) {
      final bool exchangeToken = extra[RequestOptionExtraFields.exchangeToken] ?? false;
      final String? accessToken = await getAccessToken(!exchangeToken);
      if (accessToken != null) {
        options.headers['x-autoo-access-token'] = accessToken;
      }
    }

    options.headers['x-autoo-api-version'] = httpClientParams.apiVersion;
    options.headers['x-autoo-app-type'] = httpClientParams.appType;
    options.headers['x-autoo-client-type'] = httpClientParams.deviceInfo?.type;
    options.headers['x-autoo-client-version'] = httpClientParams.appInfo?.appVersion;
    options.headers['x-autoo-client-id'] = httpClientParams.deviceInfo?.deviceId;
    options.headers['x-autoo-client-language'] = httpClientParams.deviceInfo?.locale.languageCode;
    options.headers['x-autoo-access-timestamp'] = DateFormat('yyyy-MM-dd hh:mm:ss').format((DateTime.now()));
    options.headers['Content-type'] = 'application/json';

    options.connectTimeout = extra[RequestOptionExtraFields.connectTimeout] ?? httpClientParams.connectTimeout;
    options.receiveTimeout = extra[RequestOptionExtraFields.receiveTimeout] ?? httpClientParams.receiveTimeout;
    options.sendTimeout = extra[RequestOptionExtraFields.sendTimeout] ?? httpClientParams.sendTimeout;

    handler.next(options);
  }

  void _onResponse(Response<dynamic> response, ResponseInterceptorHandler handler) {
    if (response.data is Map<String, dynamic>) {
      final Map<String, dynamic> data = response.data as Map<String, dynamic>;
      final String result = data['result'] as String;

      if (result == ServerResultCode.RESULT_SUCCESS) {
        handler.next(response);
      } else {
        handler.reject(DioError(requestOptions: response.requestOptions, response: response), true);
      }
    } else {
      handler.reject(DioError(requestOptions: response.requestOptions, response: response), true);
    }
  }

  void _onError(DioError error, ErrorInterceptorHandler handler) {
    handler.reject(HttpError.parse(error));
  }
}
