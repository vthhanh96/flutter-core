import 'package:dio/dio.dart';
import 'package:flutter_core/constant/server_const.dart';
import 'package:flutter_core/http/http_client.dart';
import 'package:flutter_core/http/http_error.dart';
import 'package:flutter_core/http/request_option_extra_fields.dart';

class RetryWhenErrorAccessTokenInterceptor extends Interceptor {
  final Dio _dio;
  final ExchangeToken _exchangeToken;

  RetryWhenErrorAccessTokenInterceptor(this._dio, this._exchangeToken);

  @override
  Future<dynamic> onError(DioError err, ErrorInterceptorHandler handler) async {
    HttpError httpError = HttpError.parse(err);
    Map<String, dynamic> extra = err.requestOptions.extra;
    final bool exchangeToken = extra[RequestOptionExtraFields.exchangeToken] ?? false;

    if (httpError.errorKey == ServerResultCode.ERROR_ACCESS_TOKEN) {
      if (exchangeToken) {
        handler.next(err);
      } else {
        try {
          await _exchangeToken();
          extra[RequestOptionExtraFields.exchangeToken] = true;
          err.requestOptions.extra = err.requestOptions.extra..addAll(extra);
          Response<dynamic> response = await _dio.fetch(err.requestOptions);
          handler.resolve(response);
        } catch (error) {
          if (error is DioError) {
            handler.reject(error);
          } else {
            rethrow;
          }
        }
      }
    } else {
      handler.next(err);
    }
  }
}
