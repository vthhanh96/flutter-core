import 'package:dio/dio.dart';
import 'package:flutter_core/http/request_option_extra_fields.dart';

class RetryInterceptor extends Interceptor {
  final Dio _dio;
  final int defaultRetryRequestCounter;

  RetryInterceptor(this._dio, {required this.defaultRetryRequestCounter});

  @override
  Future<dynamic> onError(DioError err, ErrorInterceptorHandler handler) async {
    Map<String, dynamic> extra = err.requestOptions.extra;
    final int retryCounter = extra[RequestOptionExtraFields.retryCounter] ?? defaultRetryRequestCounter;

    if (_shouldRetry(retryCounter)) {
      extra[RequestOptionExtraFields.retryCounter] = retryCounter - 1;
      err.requestOptions.extra = err.requestOptions.extra..addAll(extra);
      try {
        Response<dynamic> response = await _dio.fetch(err.requestOptions);
        handler.resolve(response);
      } catch (error)  {
        if (error is DioError) {
          handler.reject(error);
        } else {
          rethrow;
        }
      }
    } else {
      handler.next(err);
    }
  }

  bool _shouldRetry(int retryCounter) {
    return retryCounter > 0;
  }
}