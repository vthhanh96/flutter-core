import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';

import 'http_client_adapter.dart';

class DioHttpClientAdapterIO extends DioHttpClientAdapter {
  @override
  HttpClientAdapter createHttpClientAdapter() {
    return DefaultHttpClientAdapter();
  }
}
