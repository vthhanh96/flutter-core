import 'package:dio/dio.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'http_client_adapter_io.dart';

abstract class DioHttpClientAdapter extends PlatformInterface {
  DioHttpClientAdapter() : super(token: _token);

  static final Object _token = Object();

  static DioHttpClientAdapter _instance = DioHttpClientAdapterIO();

  static DioHttpClientAdapter get platform => _instance;

  static set platform(DioHttpClientAdapter instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  HttpClientAdapter createHttpClientAdapter();
}
