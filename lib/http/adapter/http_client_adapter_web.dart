import 'dart:async';
import 'dart:html';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:flutter_core/http/adapter/http_client_adapter.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';

class DioHttpClientAdapterWeb extends DioHttpClientAdapter {
  static final DioHttpClientAdapterWeb platform = DioHttpClientAdapterWeb._();

  DioHttpClientAdapterWeb._();

  static void registerWith(Registrar registrar) {
    DioHttpClientAdapter.platform = platform;
  }

  @override
  HttpClientAdapter createHttpClientAdapter() {
    return WebHttpClientAdapter();
  }
}

class WebHttpClientAdapter implements HttpClientAdapter {
  final List<HttpRequest> _xhrs = <HttpRequest>[];

  bool withCredentials = false;

  @override
  Future<ResponseBody> fetch(RequestOptions options,
      Stream<Uint8List>? requestStream, Future<dynamic>? cancelFuture) {
    HttpRequest xhr = HttpRequest();
    _xhrs.add(xhr);

    xhr
      ..open(options.method, options.uri.toString(), async: true)
      ..responseType = 'blob'
      ..withCredentials = options.extra['withCredentials'] ?? withCredentials;
    options.headers.remove(Headers.contentLengthHeader);
    options.headers.forEach((String key, dynamic v) => xhr.setRequestHeader(key, '$v'));

    Completer<ResponseBody> completer = Completer<ResponseBody>();

    xhr.onLoad.first.then((_) {
      Blob blob = xhr.response ?? Blob(<dynamic>[]);
      FileReader reader = FileReader();

      reader.onLoad.first.then((_) {
        Uint8List body = reader.result as Uint8List;
        completer.complete(
          ResponseBody(
            Stream<Uint8List>.value(body),
            xhr.status,
            headers:
            xhr.responseHeaders.map((String k, String v) => MapEntry<String, List<String>>(k, v.split(','))),
            statusMessage: xhr.statusText,
            isRedirect: xhr.status == 302 || xhr.status == 301,
          ),
        );
      });

      reader.onError.first.then((ProgressEvent error) {
        completer.completeError(
          DioError(
            type: DioErrorType.response,
            error: error,
            requestOptions: options,
          ),
          StackTrace.current,
        );
      });
      reader.readAsArrayBuffer(blob);
    });

    xhr.onError.first.then((_) {
      completer.completeError(
        DioError(
          type: DioErrorType.response,
          error: 'XMLHttpRequest error.',
          requestOptions: options,
        ),
        StackTrace.current,
      );
    });

    cancelFuture?.then((_) {
      if (xhr.readyState < 4 && xhr.readyState > 0) {
        try {
          xhr.abort();
        } catch (e) {
          // ignore
        }
      }
    });

    if (requestStream != null) {
      requestStream.toList().then((List<Uint8List> value) {
        List<int> data = <int>[];
        for(int i = 0; i < value.length; i++) {
          data.addAll(List<int>.from(value[i]));
        }
        xhr.send(Uint8List.fromList(data));
      });
    } else {
      xhr.send();
    }

    return completer.future.whenComplete(() {
      _xhrs.remove(xhr);
    });
  }

  @override
  void close({bool force = false}) {
    if (force) {
      for (HttpRequest xhr in _xhrs) {
        xhr.abort();
      }
    }
    _xhrs.clear();
  }
}
