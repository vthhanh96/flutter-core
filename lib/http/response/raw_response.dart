import 'package:flutter_core/http/response/base_response.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class RawResponse extends BaseResponse {

  @JsonKey(name: 'reply')
  Map<String, dynamic>? reply;

  RawResponse({String? result, this.reply}) : super(result: result);

  factory RawResponse.fromJson(Map<String, dynamic> json) {
    return RawResponse(
      result: json['result'] as String?,
      reply: json['reply'] as Map<String, dynamic>?,
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'result': result,
      'reply': reply,
    };
  }
}