import 'package:json_annotation/json_annotation.dart';

abstract class BaseResponse {
  @JsonKey(name: 'result')
  String? result;

  BaseResponse({this.result});
}