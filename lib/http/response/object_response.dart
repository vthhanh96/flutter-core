import 'package:flutter_core/http/response/base_response.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(genericArgumentFactories: true)
class ObjectResponse<T> extends BaseResponse {
  @JsonKey(name: 'reply')
  T? reply;

  ObjectResponse({String? result, this.reply}) : super(result: result);

  factory ObjectResponse.fromJson(Map<String, dynamic> json, T Function(Object? json) fromJsonT) {
    return ObjectResponse<T>(
      result: json['result'] as String?,
      reply: _$nullableGenericFromJson(json['reply'], fromJsonT),
    );
  }

  Map<String, dynamic> toJson(Map<String, dynamic> Function(T value) toJsonT) {
    return <String, dynamic>{
      'result': result,
      'reply': _$nullableGenericToJson(reply, toJsonT),
    };
  }
}

T? _$nullableGenericFromJson<T>(Object? input, T Function(Object? json) fromJson,) => input == null ? null : fromJson(input);

Object? _$nullableGenericToJson<T>(T? input, Object? Function(T value) toJson,) => input == null ? null : toJson(input);