import 'package:flutter/cupertino.dart';

class DeviceInfo {
  int type;
  String deviceId;
  bool isSimulator;
  bool isLargeScreen;
  Locale locale;

  DeviceInfo(this.type, this.deviceId, this.locale, {this.isSimulator = false, this.isLargeScreen = false});
}