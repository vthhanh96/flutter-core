class AppInfo {
  String appName;
  String appId;
  String appVersion;

  AppInfo(this.appName, this.appId, this.appVersion);
}