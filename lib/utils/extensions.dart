import 'dart:math';

import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;

extension StringExtension on String? {
  bool isTextEmpty() {
    if (this == null) return true;
    return this!.trim().isEmpty;
  }

  bool isTextNotEmpty() {
    if (this == null) return false;
    return this!.trim().isNotEmpty;
  }

  bool isNetwork() {
    if (this == null) return false;
    String _value = this!.trim();
    return _value.startsWith('http') || _value.startsWith('https');
  }

  bool isAsset() {
    if (this == null) return false;
    String _value = this!.trim();
    return _value.contains('asset');
  }

  bool isImagePath() {
    if (this == null) return false;
    String _value = path.extension(this!).trim();
    return _value == '.png' || _value == '.jpeg';
  }

  bool isVideoPath() {
    if (this == null) return false;
    String _value = path.extension(this!).trim();
    return _value == '.mp4';
  }

  bool isAudioPath() {
    if (this == null) return false;
    String _value = path.extension(this!).trim();
    return _value == '.mp3' || _value == '.audio';
  }

  bool isBoolean() {
    if (this == null) return false;
    String _value = this!.trim().toLowerCase();
    if (_value.isEmpty) return false;
    return _value == 'yes' || _value == 'no';
  }

  bool toBoolean() {
    if (this == null) return false;
    String _value = this!.trim().toLowerCase();
    if (_value.isEmpty) return false;
    return _value == 'yes';
  }

  num? toNum() {
    if (this == null) return null;
    String _value = this!.trim().toLowerCase();
    if (_value.isEmpty) return 0;
    if (_value.contains('.') || _value.contains(',')) return double.tryParse(_value);
    return int.tryParse(_value);
  }
}

extension NumExtension on num? {
  bool isBoolean() {
    if (this == null) return false;
    return this! == 0 || this! == 1;
  }

  bool toBoolean() {
    if (this == null) return false;
    return this! == 1;
  }
}

extension IterableExtension<E> on Iterable<E>? {
  bool isListEmpty() {
    if (this == null) return true;
    return this!.isEmpty;
  }

  bool isListNotEmpty() {
    if (this == null) return false;
    return this!.isNotEmpty;
  }
}

extension MapExtension<K, V> on Map<K, V>? {
  bool isMapEmpty() {
    if (this == null) return true;
    return this!.keys.isListEmpty();
  }

  bool isMapNotEmpty() {
    if (this == null) return false;
    return this!.keys.isListNotEmpty();
  }
}

extension DateTimeExtension on DateTime {
  DateTime applyTimeOfDay(TimeOfDay time) {
    return DateTime(year, month, day, time.hour, time.minute);
  }
}

extension DoubleExtension on double {
  double roundTo(int places) {
    num mod = pow(10.0, places);
    return ((this * mod).round().toDouble() / mod);
  }
}