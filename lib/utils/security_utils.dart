import 'dart:convert'; // for the utf8.encode method
import 'dart:math';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:hex/hex.dart';
import 'package:pointycastle/export.dart' hide Digest;

class SecurityUtils {
  static String hashPassword(String salt, String verifyCode, String password) {
    List<int> bytes = toBytes(password + salt);
    // SHA1(raw_pass + salt) = SHA1(123456XbqVJwm)
    String sha_1 = sha1.convert(bytes).toString();
    Digest sha_256 = sha256.convert(toBytes(sha_1 + verifyCode));

    // AES encrypt size 128, mode CBC (raw_pass, k)
    Uint8List iv = Uint8List(16);
    Uint8List key = bytesToUint8List(sha_256.bytes);
    CipherParameters params =
        PaddedBlockCipherParameters<ParametersWithIV<KeyParameter>, Null>(
            ParametersWithIV<KeyParameter>(KeyParameter(key), iv), null);
    PaddedBlockCipher cipher = PaddedBlockCipher('AES/CBC/PKCS7');
    cipher.init(true, params);

    Uint8List inter = cipher.process(toUint8List(password));
    return byteToHex(inter);
  }

  static String hashPasswordToRegister(String salt, String password) {
    List<int> bytes = toBytes(password + salt);
    // SHA1(raw_pass + salt) = SHA1(123456XbqVJwm)
    Digest sha_1 = sha1.convert(bytes);

    return byteToHex(sha_1.bytes);
  }

  /// Converts a string to a List<int>
  static List<int> toBytes(String str) {
    return utf8.encode(str);
  }

  /// Converts a string to a Uint8List
  static Uint8List toUint8List(String str) {
    return Uint8List.fromList(toBytes(str));
  }

  static Uint8List bytesToUint8List(List<int> bytes) {
    return Uint8List.fromList(bytes);
  }

  /// Converts a Uint8List to a hex string
  static String byteToHex(List<int> bytes) {
    return HEX.encode(bytes).toUpperCase();
  }

  /// Converts a hex string to a Uint8List
  static Uint8List hexToBytes(String hex) {
    return Uint8List.fromList(HEX.decode(hex));
  }

  /// Convert byte array to string utf-8
  static String bytesToUtf8String(Uint8List bytes) {
    return utf8.decode(bytes);
  }

  static String generateNonce([int length = 32]) {
    final String charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final Random random = Random.secure();
    return List<String>.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  /// Returns the sha256 hash of [input] in hex notation.
  static String sha256String(String input) {
    final List<int> bytes = utf8.encode(input);
    final Digest digest = sha256.convert(bytes);
    return digest.toString();
  }
}
