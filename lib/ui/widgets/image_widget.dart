import 'dart:async';
import 'dart:io' as io;
import 'dart:typed_data';
import 'dart:ui';
import 'dart:ui' as ui;

import 'package:flutter_core/utils/extensions.dart';
import 'package:flutter/foundation.dart';
import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class ImgView {
  //
  static BoxFit defaultBoxFit = BoxFit.fill;

  static Widget memory(
    Uint8List bytes, {
    Key? key,
    ImageFrameBuilder? frameBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    PlaceholderWidgetBuilder? placeholderBuilder,
    String errorSrc = '',
    String placeholderSrc = '',
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? width,
    double? height,
    Color? color,
    Color? backgroundColor,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    Alignment alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    double? scale,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
    bool? allowDrawingOutsideViewBox,
  }) {
    _setupDefaults(
      width: width,
      height: height,
      backgroundColor: backgroundColor,
      errorBuilder: errorBuilder,
      placeholderBuilder: placeholderBuilder,
      errorSrc: errorSrc,
      placeholderSrc: placeholderSrc,
    );

    return Image.memory(
      bytes,
      key: key,
      frameBuilder: frameBuilder,
      errorBuilder: (BuildContext context, Object error, __) => errorBuilder!.call(context, '', error),
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      scale: scale ?? 1.0,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  static Widget asset(
    String src, {
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    PlaceholderWidgetBuilder? placeholderBuilder,
    String errorSrc = '',
    String placeholderSrc = '',
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Color? backgroundColor,
    BlendMode colorBlendMode = BlendMode.srcIn,
    BoxFit? fit,
    Alignment alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
    bool allowDrawingOutsideViewBox = false,
  }) {
    _setupDefaults(
      width: width,
      height: height,
      backgroundColor: backgroundColor,
      errorBuilder: errorBuilder,
      placeholderBuilder: placeholderBuilder,
      errorSrc: errorSrc,
      placeholderSrc: placeholderSrc,
    );

    return Image.asset(
      src,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: (BuildContext context, Object error, __) => errorBuilder!.call(context, '', error),
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  static Widget file(
    io.File file, {
    Key? key,
    ImageFrameBuilder? frameBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    PlaceholderWidgetBuilder? placeholderBuilder,
    String errorSrc = '',
    String placeholderSrc = '',
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Color? backgroundColor,
    BlendMode colorBlendMode = BlendMode.srcIn,
    BoxFit? fit,
    Alignment alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
    bool allowDrawingOutsideViewBox = false,
  }) {
    _setupDefaults(
      width: width,
      height: height,
      backgroundColor: backgroundColor,
      errorBuilder: errorBuilder,
      placeholderBuilder: placeholderBuilder,
      errorSrc: errorSrc,
      placeholderSrc: placeholderSrc,
    );
    return Image.file(
      file,
      key: key,
      frameBuilder: frameBuilder,
      errorBuilder: (BuildContext context, Object error, __) => errorBuilder!.call(context, '', error),
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale ?? 1.0,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  static Widget icon(
    IconData icon, {
    Key? key,
    double? size,
    Color? color,
    String? semanticLabel,
    TextDirection? textDirection,
  }) {
    return Icon(
      icon,
      key: key,
      size: size,
      color: color,
      semanticLabel: semanticLabel,
      textDirection: textDirection,
    );
  }

  static Widget network(
    String src, {
    Key? key,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Color? backgroundColor,
    BlendMode colorBlendMode = BlendMode.srcIn,
    BoxFit? fit,
    Alignment alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
    bool allowDrawingOutsideViewBox = false,
    Map<String, String>? headers,
    double? ratio,
    ImageWidgetBuilder? imageBuilder,
    PlaceholderWidgetBuilder? placeholderBuilder,
    ProgressIndicatorBuilder? progressIndicatorBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    ImageFrameBuilder? frameBuilder,
    String errorSrc = '',
    String placeholderSrc = '',
    Duration? placeholderFadeInDuration,
    Duration fadeOutDuration = const Duration(milliseconds: 1000),
    Curve fadeOutCurve = Curves.easeOut,
    Duration fadeInDuration = const Duration(milliseconds: 500),
    Curve fadeInCurve = Curves.easeIn,
    bool useOldImageOnUrlChange = false,
    ImageRenderMethodForWeb? imageRenderMethodForWeb,
    BaseCacheManager? cacheManager,
    bool enableCacheNetworkImage = true,
  }) {
    _setupDefaults(
      width: width,
      height: height,
      backgroundColor: backgroundColor,
      errorBuilder: errorBuilder,
      placeholderBuilder: placeholderBuilder,
      errorSrc: errorSrc,
      placeholderSrc: placeholderSrc,
    );

    if (enableCacheNetworkImage) {
      return CachedNetworkImage(
        key: key,
        cacheManager: cacheManager ?? GetIt.instance.get<BaseCacheManager>(),
        imageRenderMethodForWeb: imageRenderMethodForWeb,
        useOldImageOnUrlChange: useOldImageOnUrlChange,
        fadeInCurve: fadeInCurve,
        fadeInDuration: fadeInDuration,
        fadeOutCurve: fadeOutCurve,
        fadeOutDuration: fadeOutDuration,
        progressIndicatorBuilder: progressIndicatorBuilder,
        placeholderFadeInDuration: placeholderFadeInDuration,
        placeholder: placeholderBuilder,
        imageUrl: src,
        imageBuilder: imageBuilder,
        errorWidget: errorBuilder,
        width: width,
        height: height,
        color: color,
        colorBlendMode: colorBlendMode,
        fit: fit,
        alignment: alignment,
        repeat: repeat,
        matchTextDirection: matchTextDirection,
        httpHeaders: headers,
        filterQuality: filterQuality,
        memCacheWidth: cacheWidth,
        memCacheHeight: cacheHeight,
      );
    }

    return Image.network(
      src,
      key: key,
      frameBuilder: frameBuilder,
      errorBuilder: (BuildContext context, Object error, __) => errorBuilder!.call(context, '', error),
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale ?? 1.0,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      headers: headers,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  static Widget load(
    String src, {
    Key? key,
    ImageFrameBuilder? frameBuilder,
    ImageWidgetBuilder? imageBuilder,
    PlaceholderWidgetBuilder? placeholderBuilder,
    ProgressIndicatorBuilder? progressIndicatorBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    String errorSrc = '',
    String placeholderSrc = '',
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Color? backgroundColor,
    BlendMode colorBlendMode = BlendMode.srcIn,
    BoxFit? fit = BoxFit.fill,
    Alignment alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
    bool allowDrawingOutsideViewBox = false,
    Map<String, String>? headers,
    String? package,
    AssetBundle? bundle,
    double? ratio,
    Duration? placeholderFadeInDuration,
    Duration fadeOutDuration = const Duration(milliseconds: 1000),
    Curve fadeOutCurve = Curves.easeOut,
    Duration fadeInDuration = const Duration(milliseconds: 500),
    Curve fadeInCurve = Curves.easeIn,
    bool useOldImageOnUrlChange = false,
    ImageRenderMethodForWeb? imageRenderMethodForWeb,
    BaseCacheManager? cacheManager,
    bool enableCacheNetworkImage = true,
    ImageStyle? style,
  }) {
    style = style?.copyWith(color: color, backgroundColor: backgroundColor);

    Widget _img() {
      return _load(
        src,
        key: key,
        width: width,
        height: height,
        frameBuilder: frameBuilder,
        errorBuilder: errorBuilder,
        semanticLabel: semanticLabel,
        excludeFromSemantics: excludeFromSemantics,
        scale: scale,
        color: color,
        colorBlendMode: colorBlendMode,
        fit: fit,
        alignment: alignment,
        repeat: repeat,
        centerSlice: centerSlice,
        matchTextDirection: matchTextDirection,
        gaplessPlayback: gaplessPlayback,
        isAntiAlias: isAntiAlias,
        headers: headers,
        filterQuality: filterQuality,
        cacheWidth: cacheWidth,
        cacheHeight: cacheHeight,
        ratio: ratio,
        imageBuilder: imageBuilder,
        allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
        cacheManager: cacheManager,
        imageRenderMethodForWeb: imageRenderMethodForWeb,
        useOldImageOnUrlChange: useOldImageOnUrlChange,
        fadeInCurve: fadeInCurve,
        fadeInDuration: fadeInDuration,
        fadeOutCurve: fadeOutCurve,
        fadeOutDuration: fadeOutDuration,
        progressIndicatorBuilder: progressIndicatorBuilder,
        placeholderFadeInDuration: placeholderFadeInDuration,
        placeholderBuilder: placeholderBuilder,
        errorSrc: errorSrc,
        placeholderSrc: placeholderSrc,
        enableCacheNetworkImage: enableCacheNetworkImage,
        style: style,
        package: package,
      );
    }

    Widget _shapeImg() {
      if (null != style?.shape) {
        if (style!.shape == BoxShape.circle) {
          return ClipOval(child: _img());
        } else {
          BorderRadius? borderRadius = style.borderRadius ?? (style.decoration?.borderRadius as BorderRadius?);
          if (null != borderRadius) {
            return ClipRRect(borderRadius: borderRadius, child: _img());
          }
        }
      }
      return _img();
    }

    BoxDecoration decoration = style?.decoration ??
        BoxDecoration(
          border: style?.border ?? Border.all(color: style?.borderColor ?? Colors.black12),
          borderRadius: style?.borderRadius ?? BorderRadius.zero,
          color: style?.backgroundColor,
          shape: style?.shape ?? BoxShape.rectangle,
          boxShadow: style?.boxShadow,
          gradient: style?.gradient,
          backgroundBlendMode: style?.backgroundBlendMode,
        );

    if (null != decoration.borderRadius) {
      decoration = decoration.copyWith(borderRadius: decoration.borderRadius?.add(BorderRadius.circular(1)));
    }

    return Container(
      key: key,
      padding: style?.padding,
      margin: style?.margin,
      decoration: decoration,
      child: _shapeImg(),
    );
  }

  static Widget _load(
    String src, {
    Key? key,
    ImageFrameBuilder? frameBuilder,
    ImageWidgetBuilder? imageBuilder,
    PlaceholderWidgetBuilder? placeholderBuilder,
    ProgressIndicatorBuilder? progressIndicatorBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    String errorSrc = '',
    String placeholderSrc = '',
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Color? backgroundColor,
    BlendMode colorBlendMode = BlendMode.srcIn,
    BoxFit? fit = BoxFit.fill,
    Alignment alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
    bool allowDrawingOutsideViewBox = false,
    Map<String, String>? headers,
    String? package,
    AssetBundle? bundle,
    double? ratio,
    Duration? placeholderFadeInDuration,
    Duration fadeOutDuration = const Duration(milliseconds: 1000),
    Curve fadeOutCurve = Curves.easeOut,
    Duration fadeInDuration = const Duration(milliseconds: 500),
    Curve fadeInCurve = Curves.easeIn,
    bool useOldImageOnUrlChange = false,
    ImageRenderMethodForWeb? imageRenderMethodForWeb,
    BaseCacheManager? cacheManager,
    bool enableCacheNetworkImage = true,
    ImageStyle? style,
  }) {
    style = style?.copyWith(color: color, backgroundColor: backgroundColor);
    if (src.isNetwork()) {
      return network(
        src,
        key: key,
        frameBuilder: frameBuilder,
        errorBuilder: errorBuilder,
        semanticLabel: semanticLabel,
        excludeFromSemantics: excludeFromSemantics,
        scale: scale,
        width: width,
        height: height,
        color: style?.color,
        backgroundColor: style?.backgroundColor,
        colorBlendMode: colorBlendMode,
        fit: fit,
        alignment: alignment,
        repeat: repeat,
        centerSlice: centerSlice,
        matchTextDirection: matchTextDirection,
        gaplessPlayback: gaplessPlayback,
        isAntiAlias: isAntiAlias,
        headers: headers,
        filterQuality: filterQuality,
        cacheWidth: cacheWidth,
        cacheHeight: cacheHeight,
        ratio: ratio,
        imageBuilder: imageBuilder,
        allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
        cacheManager: cacheManager,
        imageRenderMethodForWeb: imageRenderMethodForWeb,
        useOldImageOnUrlChange: useOldImageOnUrlChange,
        fadeInCurve: fadeInCurve,
        fadeInDuration: fadeInDuration,
        fadeOutCurve: fadeOutCurve,
        fadeOutDuration: fadeOutDuration,
        progressIndicatorBuilder: progressIndicatorBuilder,
        placeholderFadeInDuration: placeholderFadeInDuration,
        placeholderBuilder: placeholderBuilder,
        placeholderSrc: placeholderSrc,
        errorSrc: errorSrc,
        enableCacheNetworkImage: enableCacheNetworkImage,
      );
    } else if (src.isAsset()) {
      return asset(
        src,
        key: key,
        bundle: bundle,
        frameBuilder: frameBuilder,
        errorBuilder: errorBuilder,
        semanticLabel: semanticLabel,
        excludeFromSemantics: excludeFromSemantics,
        placeholderBuilder: placeholderBuilder,
        scale: scale,
        width: width,
        placeholderSrc: placeholderSrc,
        errorSrc: errorSrc,
        height: height,
        colorBlendMode: colorBlendMode,
        fit: fit,
        alignment: alignment,
        repeat: repeat,
        centerSlice: centerSlice,
        matchTextDirection: matchTextDirection,
        gaplessPlayback: gaplessPlayback,
        isAntiAlias: isAntiAlias,
        package: package,
        filterQuality: filterQuality,
        cacheWidth: cacheWidth,
        cacheHeight: cacheHeight,
        allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
        color: style?.color,
        backgroundColor: style?.backgroundColor,
      );
    } else {
      return file(
        io.File(src),
        key: key,
        frameBuilder: frameBuilder,
        errorBuilder: errorBuilder,
        semanticLabel: semanticLabel,
        excludeFromSemantics: excludeFromSemantics,
        scale: scale,
        width: width,
        height: height,
        placeholderSrc: placeholderSrc,
        errorSrc: errorSrc,
        color: style?.color,
        backgroundColor: style?.backgroundColor,
        colorBlendMode: colorBlendMode,
        fit: fit,
        alignment: alignment,
        repeat: repeat,
        centerSlice: centerSlice,
        matchTextDirection: matchTextDirection,
        gaplessPlayback: gaplessPlayback,
        isAntiAlias: isAntiAlias,
        filterQuality: filterQuality,
        cacheWidth: cacheWidth,
        cacheHeight: cacheHeight,
        placeholderBuilder: placeholderBuilder,
        allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      );
    }
  }

  static void _setupDefaults({
    double? width,
    double? height,
    String? errorSrc,
    String? placeholderSrc,
    PlaceholderWidgetBuilder? placeholderBuilder,
    LoadingErrorWidgetBuilder? errorBuilder,
    Color? backgroundColor,
    ImageStyle? style,
  }) {
    errorBuilder ??= (BuildContext context, String url, dynamic error) {
      return _defaultImageHolder(
        src: errorSrc,
        width: width,
        height: height,
        style: style,
        backgroundColor: backgroundColor,
      );
    };

    placeholderBuilder ??= (BuildContext context, String url) {
      return _defaultImageHolder(
        src: placeholderSrc,
        width: width,
        height: height,
        style: style,
        backgroundColor: backgroundColor,
      );
    };
  }

  static Widget _defaultImageHolder({
    String? src,
    double? width,
    double? height,
    ImageStyle? style,
    Color? backgroundColor,
  }) {
    Widget _imgHolder() {
      if (src.isTextNotEmpty()) {
        try {
          if (src!.isNetwork()) {
            return CachedNetworkImage(
              imageUrl: src,
              width: width,
              height: height,
              fit: BoxFit.fill,
            );
          } else {
            return Image.asset(
              src,
              width: width,
              height: height,
              fit: BoxFit.fill,
            );
          }
        } catch (e, stackTrace) {
          GetIt.instance<Logger>().e('ImageHolder', e, stackTrace);
        }
      }

      return Container(
        width: width,
        height: height,
        color: style?.backgroundColor ?? backgroundColor ?? Colors.black12,
      );
    }

    if (null != style) {
      return Container(
        decoration: style.decoration ??
            BoxDecoration(
              color: style.backgroundColor,
              shape: style.shape,
              borderRadius: style.borderRadius,
              border: style.border,
              boxShadow: style.boxShadow,
              gradient: style.gradient,
              backgroundBlendMode: style.backgroundBlendMode,
            ),
        child: _imgHolder(),
      );
    }
    return _imgHolder();
  }

  static Future<Uint8List?> readAsBytes(
    String src, {
    double? size,
    bool resizeFromCanvas = false,
  }) async {
    if (src.isTextEmpty()) return null;
    String? extension = src.split('.').last;
    Uint8List? bytes = await _readSrcAsBytes(src: src);
    bytes = await resize(bytes: bytes, size: size, extension: extension, resizeFromCanvas: resizeFromCanvas);
    return bytes;
  }

  static Future<Uint8List?> resize({
    Uint8List? bytes,
    double? size,
    bool resizeFromCanvas = false,
    String? extension,
  }) async {
    if (null == bytes) return null;

    try {
      ui.Image? x;
      if (resizeFromCanvas && null != size && size > 0) {
        final Uint8List? resizedImageBytes = await getBytesFromCanvas(size.toInt(), size.toInt(), bytes);
        return resizedImageBytes;
      }

      if ((null != size && size > 0) || (extension != '.png' && extension != '.jpeg' && extension != '.jpg')) {
        try {
          x = await decodeImageFromList(bytes);
        } catch (e, stackTrace) {
          GetIt.instance<Logger>().e('ImgView', e, stackTrace);
        }

        double _size = size ?? x?.width.toDouble() ?? 1024.0;
        if (null != x) {
          if (_size > x.width) {
            size = x.width.toDouble();
          }
          if (x.width <= _size && (extension == '.png' || extension == '.jpeg' && extension != '.jpg')) {
            return bytes;
          }
        }
        final Codec imageCodec = await instantiateImageCodec(bytes, targetWidth: _size.toInt(), allowUpscaling: false);
        final FrameInfo frameInfo = await imageCodec.getNextFrame();
        final ByteData? byteData = await frameInfo.image.toByteData(format: ImageByteFormat.png);
        final Uint8List? resizedImageBytes = byteData?.buffer.asUint8List();
        return resizedImageBytes;
      }
    } catch (e, stackTrace) {
      GetIt.instance<Logger>().e('ImgView', e, stackTrace);
    }
    return bytes;
  }

  static Future<Uint8List?> getBytesFromCanvas(int width, int height, Uint8List dataBytes) async {
    final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint = Paint()..color = Colors.transparent;
    final Radius radius = Radius.circular(20.0);

    canvas.drawRRect(
        RRect.fromRectAndCorners(
          Rect.fromLTWH(0.0, 0.0, width.toDouble(), height.toDouble()),
          topLeft: radius,
          topRight: radius,
          bottomLeft: radius,
          bottomRight: radius,
        ),
        paint);

    ui.Image imaged = await readImageFromBytes(dataBytes.buffer.asUint8List());
    canvas.drawImageRect(
      imaged,
      Rect.fromLTRB(0.0, 0.0, imaged.width.toDouble(), imaged.height.toDouble()),
      Rect.fromLTRB(0.0, 0.0, width.toDouble(), height.toDouble()),
      Paint(),
    );

    final ui.Image img = await pictureRecorder.endRecording().toImage(width, height);
    final ByteData? data = await img.toByteData(format: ui.ImageByteFormat.png);
    return data?.buffer.asUint8List();
  }

  static Future<ui.Image> readImageFromBytes(List<int> bytes) async {
    final Completer<ui.Image> completer = Completer<ui.Image>();
    ui.decodeImageFromList(Uint8List.fromList(bytes), (ui.Image img) {
      return completer.complete(img);
    });
    return completer.future;
  }

  static Future<Uint8List?> _readSrcAsBytes({required String src}) async {
    Uint8List? bytes;
    try {
      if (src.isTextNotEmpty()) {
        if (src.isNetwork()) {
          io.File file = await DefaultCacheManager().getSingleFile(src);
          bytes = await file.readAsBytes();
        } else if (src.isAsset()) {
          bytes = (await rootBundle.load(src)).buffer.asUint8List();
        } else {
          io.File imageFile = io.File(src);
          if (await imageFile.exists()) {
            bytes = await imageFile.readAsBytes();
          } else {
            bytes = (await rootBundle.load(src)).buffer.asUint8List();
          }
        }
      }
    } catch (e, stackTrace) {
      GetIt.instance<Logger>().e('ImgView', e, stackTrace);
    }
    return bytes;
  }
}

@immutable
class ImageStyle with Diagnosticable {
  const ImageStyle({
    this.padding,
    this.margin,
    this.color,
    this.border,
    this.shape = BoxShape.rectangle,
    this.decoration,
    this.borderColor,
    this.foregroundColor,
    this.backgroundColor,
    this.borderRadius,
    this.boxShadow,
    this.gradient,
    this.backgroundBlendMode,
  });

  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final Color? color;
  final Color? borderColor;
  final Color? foregroundColor;
  final Color? backgroundColor;
  final BorderRadius? borderRadius;
  final BoxShape shape;
  final BoxBorder? border;
  final BoxDecoration? decoration;
  final List<BoxShadow>? boxShadow;
  final Gradient? gradient;
  final BlendMode? backgroundBlendMode;

  ImageStyle copyWith({
    EdgeInsets? padding,
    EdgeInsets? margin,
    BoxDecoration? decoration,
    Color? color,
    Color? borderColor,
    Color? foregroundColor,
    Color? backgroundColor,
    BorderRadius? borderRadius,
    BoxShape? shape,
    BoxBorder? border,
    List<BoxShadow>? boxShadow,
    Gradient? gradient,
    BlendMode? backgroundBlendMode,
  }) {
    return ImageStyle(
      padding: padding ?? this.padding,
      margin: margin ?? this.margin,
      color: color ?? this.color,
      foregroundColor: foregroundColor ?? this.foregroundColor,
      border: border ?? this.border,
      shape: shape ?? this.shape,
      borderRadius: borderRadius ?? this.borderRadius,
      decoration: decoration ?? this.decoration,
      borderColor: borderColor ?? this.borderColor,
      backgroundColor: backgroundColor ?? this.backgroundColor,
      boxShadow: boxShadow ?? this.boxShadow,
      gradient: gradient ?? this.gradient,
      backgroundBlendMode: backgroundBlendMode ?? this.backgroundBlendMode,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (other.runtimeType != runtimeType) {
      return false;
    }

    return other is ImageStyle &&
        other.border == border &&
        other.color == color &&
        other.borderRadius == borderRadius &&
        other.borderColor == borderColor &&
        other.padding == padding &&
        other.margin == margin &&
        other.foregroundColor == foregroundColor &&
        other.backgroundColor == backgroundColor &&
        other.shape == shape &&
        other.boxShadow == boxShadow &&
        other.gradient == gradient &&
        other.backgroundBlendMode == backgroundBlendMode &&
        other.decoration == decoration;
  }

  @override
  int get hashCode {
    return hashValues(
      color,
      borderColor,
      margin,
      padding,
      borderRadius,
      decoration,
      foregroundColor,
      backgroundColor,
      border,
      shape,
      backgroundBlendMode,
      gradient,
      boxShadow,
    );
  }

  @override
  String toStringShort() => objectRuntimeType(this, 'ImageStyle');
}
