import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {

  final Color color;

  LoadingWidget({required this.color});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              color: const Color.fromARGB(200, 255, 255, 255),
            ),
          ),
          Center(
            child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(color)),
          )
        ],
      ),
    );
  }
}
