import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_core/ui/base_view_state.dart';
import 'package:flutter_core/ui/widgets/loading_widget.dart';
import 'package:rxdart/rxdart.dart';

enum PageState { IDLE, LOADING }

class LoadableWidget extends StatelessWidget {
  final Widget child;
  final LoadableBloc bloc;
  final Widget? loadingWidget;

  LoadableWidget({required this.bloc, required this.child, this.loadingWidget});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        child,
        Positioned.fill(
          child: StreamBuilder<PageState>(
              stream: bloc.pageStateStream,
              builder: (_, AsyncSnapshot<PageState> snapshot) {
                final PageState? result = snapshot.data;
                if (result == PageState.LOADING) return loadingWidget ?? LoadingWidget(color: Colors.blue);
                return Container();
              }),
        ),
      ],
    );
  }
}

mixin LoadableBloc on BaseViewBloc {
  final BehaviorSubject<PageState> _pageStateController = BehaviorSubject<PageState>.seeded(PageState.IDLE);

  Stream<PageState> get pageStateStream => _pageStateController.stream;

  @protected
  void showLoading() {
    _pageStateController.sink.add(PageState.LOADING);
  }

  @protected
  void hideLoading() {
    _pageStateController.sink.add(PageState.IDLE);
  }

  @override
  void disposeState() {
    _pageStateController.close();
    super.disposeState();
  }
}
