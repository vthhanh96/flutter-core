import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:rxdart/rxdart.dart';

abstract class BaseEvent {}

abstract class BaseViewState<T extends StatefulWidget, B extends BaseViewBloc> extends State<T> {
  final B bloc = GetIt.instance<B>();

  @protected
  void loadArguments() {}

  @protected
  void onBlocEventListener(BaseEvent event) {}

  @override
  void initState() {
    super.initState();
    loadArguments();
    bloc.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) => bloc.eventStream.listen(onBlocEventListener));
  }

  @override
  void didUpdateWidget(covariant T oldWidget) {
    super.didUpdateWidget(oldWidget);
    loadArguments();
  }

  @override
  void dispose() {
    bloc.disposeState();
    super.dispose();
  }
}

abstract class BaseViewBloc {
  bool _isDisposed = false;

  final BehaviorSubject<BaseEvent> _eventStreamController = BehaviorSubject<BaseEvent>();

  Stream<BaseEvent> get eventStream => _eventStreamController.stream;

  Sink<BaseEvent> get eventSink => _eventStreamController.sink;

  bool get isDisposed => _isDisposed;

  void initState() {}

  @mustCallSuper
  void disposeState() {
    _isDisposed = true;
    _eventStreamController.close();
  }

  @protected
  Future<dynamic> call(
    dynamic Function() handler, {
    dynamic Function()? onStart,
    dynamic Function()? onFinish,
    dynamic Function()? onSuccess,
    dynamic Function(dynamic error)? onFailure,
  }) async {
    bool success = true;
    try {
      onStart?.call();
      final dynamic result = handler();
      if (result is Future) {
        await result;
      }
    } catch (error) {
      if (_isDisposed) return;
      success = false;
      final dynamic result = onFailure?.call(error);
      if (result is Future) {
        await result;
      }
    } finally {
      if (_isDisposed) return;
      onFinish?.call();
      if (success) {
        final dynamic result = onSuccess?.call();
        if (result is Future) {
          await result;
        }
      }
    }
  }

  @protected
  T? syncCall<T>(T Function() handler, {dynamic Function()? onSuccess, dynamic Function(dynamic error)? onFailure}) {
    T? result;
    try {
      result = handler();
      onSuccess?.call();
    } catch (error) {
      onFailure?.call(error);
    }
    return result;
  }
}
