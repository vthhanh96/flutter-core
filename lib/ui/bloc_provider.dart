import 'package:flutter/material.dart';
import 'package:flutter_core/ui/base_view_state.dart';

class BlocProvider<T extends BaseViewBloc> extends InheritedWidget {

  final T bloc;

  BlocProvider({required this.bloc, required Widget child}) : super(child: child);

  static T? of<T extends BaseViewBloc>(BuildContext context) {
    final BlocProvider<T>? blocProvider = context.dependOnInheritedWidgetOfExactType<BlocProvider<T>>();
    return blocProvider?.bloc;
  }

  @override
  bool updateShouldNotify(covariant BlocProvider<T> oldWidget) => bloc != oldWidget.bloc;
}

class MultiBlocProvider extends InheritedWidget {
  final List<BaseViewBloc> blocs;

  MultiBlocProvider({required this.blocs, required Widget child}) : super(child: child);

  static T? of<T extends BaseViewBloc>(BuildContext context) {
    final MultiBlocProvider? multiBlocProvider = context.dependOnInheritedWidgetOfExactType<MultiBlocProvider>();
    if (multiBlocProvider == null) {
      return null;
    }

    for (BaseViewBloc bloc in multiBlocProvider.blocs) {
      if (bloc is T) {
        return bloc;
      }
    }

    return null;
  }

  @override
  bool updateShouldNotify(covariant MultiBlocProvider oldWidget) => blocs != oldWidget.blocs;

}