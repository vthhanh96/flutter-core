import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core/router/route_data.dart';
import 'package:flutter_core/router/tree_router.dart';
import 'package:get_it/get_it.dart';

class PageRouter {

  final PageRouterDelegate routerDelegate;
  final PageRouterInformationParser informationParser;

  PageRouter._(TreeRouter treeRouter, String initRouteName)
      : routerDelegate = PageRouterDelegate(treeRouter, initialRouteName: initRouteName),
        informationParser = PageRouterInformationParser(treeRouter);

  factory PageRouter({required String initRouteName, required Map<String, RoutePath> routes, RoutePath? unknownRoutePath}) =>
      PageRouter._(_createTreeRouter(routes, unknownRoutePath), initRouteName);

  static TreeRouter _createTreeRouter(Map<String, RoutePath> routes, RoutePath? unknownPageRoutePath) {
    TreeRouter treeRouter = TreeRouter(unknownRoutePath: unknownPageRoutePath);
    for (String key in routes.keys) {
      treeRouter.add(key, routes[key]!);
    }
    return treeRouter;
  }

  static BuildContext? get currentContext => GetIt.instance<GlobalKey<NavigatorState>>().currentContext;

  static PageRouterDelegate of(BuildContext context) {
    final RouterDelegate<dynamic> delegate = Router.of(context).routerDelegate;
    assert(delegate is PageRouterDelegate);
    return delegate as PageRouterDelegate;
  }
}

class PageRouterDelegate extends RouterDelegate<RouteData> with ChangeNotifier, PopNavigatorRouterDelegateMixin<RouteData> {
  final GlobalKey<NavigatorState> _globalKey = GetIt.instance<GlobalKey<NavigatorState>>();
  final TreeRouter _treeRouter;

  final List<RouteData> _routeStack = <RouteData>[];

  PageRouterDelegate(this._treeRouter, {required String initialRouteName}) {
    _routeStack.add(_treeRouter.get(initialRouteName));
  }

  @override
  GlobalKey<NavigatorState> get navigatorKey => _globalKey;

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _globalKey,
      pages: <Page<dynamic>>[
        ..._routeStack.map((RouteData routeData) => MaterialPage<dynamic>(child: routeData.routePath.builder(context, routeData.parameters)))
      ],
      reportsRouteUpdateToEngine: true,
      transitionDelegate: kIsWeb ? _NoAnimationTransitionDelegate<dynamic>() : DefaultTransitionDelegate<dynamic>(),
      onPopPage: (Route<dynamic> route, dynamic result) {
        if (!route.didPop(result)) {
          return false;
        }

        _routeStack.removeLast();
        notifyListeners();
        return true;
      },
    );
  }

  @override
  RouteData? get currentConfiguration => _routeStack.last;

  @override
  Future<void> setInitialRoutePath(RouteData configuration) async {
    if (kIsWeb) {
      _routeStack.clear();
      _routeStack.add(configuration);
    }
    return SynchronousFuture<void>(null);
  }

  @override
  Future<void> setNewRoutePath(RouteData configuration) async {
    _routeStack.clear();
    _routeStack.add(configuration);
    return SynchronousFuture<void>(null);
  }

  Future<void> pushNamed(String routeName) async {
    _routeStack.add(_treeRouter.get(routeName));
    notifyListeners();
  }

  Future<void> replaceNamed(String routeName) async {
    _routeStack.removeLast();
    _routeStack.add(_treeRouter.get(routeName));
    notifyListeners();
  }

  Future<void> replaceAllNamed(String routeName) async {
    _routeStack.clear();
    _routeStack.add(_treeRouter.get(routeName));
    notifyListeners();
  }

  Future<void> pop() async {
    if (_routeStack.length == 1) return;
    _routeStack.removeLast();
    notifyListeners();
  }
}

class PageRouterInformationParser extends RouteInformationParser<RouteData> {
  final TreeRouter _treeRouter;

  PageRouterInformationParser(this._treeRouter);

  @override
  Future<RouteData> parseRouteInformation(RouteInformation routeInformation) async {
    final String path = routeInformation.location ?? '';
    final RouteData routeData = _treeRouter.get(path);

    RoutePath routePath = routeData.routePath;

    if (await routePath.shouldRedirect?.call() == true && routePath.redirectTo != null) {
      final RouteData redirectRouteData = _treeRouter.get(routePath.redirectTo!);
      redirectRouteData.isRedirect = true;
      return redirectRouteData;
    }
    return routeData;
  }

  @override
  RouteInformation? restoreRouteInformation(RouteData configuration) {
    return RouteInformation(location: configuration.path);
  }
}

class _NoAnimationTransitionDelegate<T> extends TransitionDelegate<T> {
  @override
  Iterable<RouteTransitionRecord> resolve({
    required List<RouteTransitionRecord> newPageRouteHistory,
    required Map<RouteTransitionRecord?, RouteTransitionRecord> locationToExitingPageRoute,
    required Map<RouteTransitionRecord?, List<RouteTransitionRecord>> pageRouteToPagelessRoutes,
  }) {
    final List<RouteTransitionRecord> results = <RouteTransitionRecord>[];

    for (final RouteTransitionRecord pageRoute in newPageRouteHistory) {
      if (pageRoute.isWaitingForEnteringDecision) {
        pageRoute.markForAdd();
      }
      results.add(pageRoute);
    }
    for (final RouteTransitionRecord exitingPageRoute in locationToExitingPageRoute.values) {
      if (exitingPageRoute.isWaitingForExitingDecision) {
        exitingPageRoute.markForRemove();
        final List<RouteTransitionRecord>? pagelessRoutes = pageRouteToPagelessRoutes[exitingPageRoute];
        if (pagelessRoutes != null) {
          for (final RouteTransitionRecord pagelessRoute in pagelessRoutes) {
            if (pagelessRoute.isWaitingForExitingDecision) {
              pagelessRoute.markForRemove();
            }
          }
        }
      }
      results.add(exitingPageRoute);
    }
    return results;
  }
}
