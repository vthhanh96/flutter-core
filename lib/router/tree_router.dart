import 'package:flutter/material.dart';
import 'package:flutter_core/router/route_data.dart';

class TreeRouter {
  final TreeNode<String?, RoutePath?> _root = TreeNode<String?, RoutePath?>(null, null);
  RoutePath _unknownRoutePath = defaultUnknownRoutePath;

  TreeRouter({RoutePath? unknownRoutePath}) {
    if (unknownRoutePath != null) {
      _unknownRoutePath = unknownRoutePath;
    }
  }

  void add(String path, RoutePath routePath) {
    final List<String> pathSegments = Uri.tryParse(path)?.pathSegments ?? <String>[];
    TreeNode<String?, RoutePath?> currentNode = _root;

    for (String segment in pathSegments) {
      if (!currentNode.contains(segment)) {
        currentNode.add(segment, null);
      }
      currentNode = currentNode.get(segment)!;
    }

    currentNode.value = routePath;
  }

  RouteData get(String path) {
    final Uri? uri = Uri.tryParse(path);
    final List<String> pathSegments = uri?.pathSegments ?? <String>[];
    Map<String, String> parameters = <String, String>{};
    parameters.addAll(uri?.queryParameters ?? <String, String>{});
    TreeNode<String?, RoutePath?> currentNode = _root;

    for (String segment in pathSegments) {
      if (currentNode.contains(segment)) {
        currentNode = currentNode.get(segment)!;
      } else if (currentNode.containsWhere((String? k) => k?.startsWith(':') == true)) {
        currentNode = currentNode.getWhere((String? k) => k?.startsWith(':') == true)!;
        parameters[currentNode.key!] = segment;
      } else {
        return RouteData(path: path, routePath: _unknownRoutePath, parameters: parameters);
      }
    }

    if (currentNode.value == null) return RouteData(path: path, routePath: _unknownRoutePath, parameters: parameters);

    return RouteData(path: path, routePath: currentNode.value!, parameters: parameters);
  }
}

class TreeNode<K, V> {
  K? key;
  V? value;
  final Map<K?, TreeNode<K?, V?>> _children;

  TreeNode(this.key, this.value) : _children = <K?, TreeNode<K?, V?>>{};

  bool contains(K? key) {
    return _children.containsKey(key);
  }

  void add(K? key, V? value) {
    _children[key] = TreeNode<K?, V?>(key, value);
  }

  TreeNode<K?, V?>? get(K key) {
    return _children[key];
  }

  bool containsWhere(bool Function(K? k) test) {
    for (K? childKey in _children.keys) {
      if (test(childKey)) {
        return true;
      }
    }
    return false;
  }

  TreeNode<K?, V?>? getWhere(bool Function(K? k) test) {
    for (K? childKey in _children.keys) {
      if (test(childKey)) {
        return _children[childKey];
      }
    }
    return null;
  }
}

RoutePath defaultUnknownRoutePath = RoutePath(
    builder: (_, __) => Container(
      child: Scaffold(
        body: Container(
          child: Center(
            child: Text('404 Not Found'),
          ),
        ),
      ),
    ));
