import 'package:flutter/material.dart';

typedef PageWidgetBuilder = Widget Function(
  BuildContext context,
  Map<String, String> parameters,
);

typedef Redirect = Future<bool> Function();

class RoutePath {
  PageWidgetBuilder builder;
  Redirect? shouldRedirect;
  String? redirectTo;

  RoutePath({required this.builder, this.shouldRedirect, this.redirectTo});
}

class RouteData {
  String path;
  RoutePath routePath;
  Map<String, String> parameters;
  bool isRedirect;

  RouteData({required this.path, required this.routePath, required this.parameters, this.isRedirect = false});
}
