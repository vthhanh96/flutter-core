class Cache {
  final Map<String, dynamic> _data = <String, dynamic>{};

  void set(String key, dynamic data) {
    _data[key] = data;
  }

  T? get<T>(String key) {
    if (_data.containsKey(key)) {
      dynamic data = _data[key];
      if (data is T) {
        return data;
      }
    }
    return null;
  }

  void remove(String key) {
    _data.remove(key);
  }

  void removeAll() {
    _data.clear();
  }
}