class AppType {
  static const int AUTH = 10;
  static const int USER_APP = 11;
  static const int ADMIN = 12;
  static const int ADMIN_SERVICE = 14;
}

class ClientType {
  static const int CLIENT_WEB = 1;
  static const int CLIENT_IOS = 2;
  static const int CLIENT_ANDROID = 3;
}

class HttpCode {
  static const int SUCCESS = 200;
  static const int UNKNOWN = -111;
  static const int BAD_GATEWAY = 502;
}

class NetworkRequestMethod {
  static const String POST = 'POST';
  static const String GET = 'GET';
}

class ServerResultCode {
  static const String RESULT_SUCCESS = 'success';
  static const String ERROR_DEFINED_MESSAGE = 'error_defined_message';

  static const String ERROR_ACCESS_TOKEN = 'error_access_token';
  static const String ERROR_AUTH = 'error_auth';

  static const String ERROR_NETWORK = 'error_network';
  static const String ERROR_WRONG_PARAMS = 'error_params';
  static const String ERROR_HEADER = 'error_header';
  static const String ERROR_FORBIDDEN = 'error_forbidden';
  static const String ERROR_VERSION_TOO_LOW = 'error_version_too_low';
  static const String ERROR_RETURN_FORCE_LOGOUT = 'error_force_logout';
  static const String ERROR_DATA_NOT_EXISTS = 'error_data_not_exists';
  static const String ERROR_APP = 'error_app';
  static const String ERROR_APP_KEY = 'error_app_key';
  static const String ERROR_SIGNATURE = 'error_signature';
  static const String WARNING_MESSAGE = 'warning_message';

  static const String FORCE_MIGRATE = 'force_migrate';
  static const String FORCE_UPDATE_APP = 'force_update_app';
}
